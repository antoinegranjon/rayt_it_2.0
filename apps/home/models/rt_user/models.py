from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.db import models


class RTUserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
            **extra_fields,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
            **extra_fields,
        )
        user.save(using=self._db)
        return user


class RTUser(AbstractBaseUser):
    class Meta:
        verbose_name = 'Utilisateur'
        verbose_name_plural = 'Utilisateurs'

    # Fields
    email = models.EmailField(
        'Adresse e-mail',
        max_length=255,
        unique=True,
    )
    pseudo = models.CharField(
        'Pseudo',
        max_length=150
    )
    valid_password = models.BooleanField(default=True)
    is_active = models.BooleanField(
        'Utilisateur actif ?',
        default=True,
    )

    objects = RTUserManager()

    def __str__(self):
        return self.pseudo

