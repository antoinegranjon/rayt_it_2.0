from django.db import models

from apps.home.models.enumeration.models import TvType, TvGender
from apps.home.models.rt_user.models import RTUser


class Tv(models.Model):
    # Fields
    movie_id = models.CharField(
        'Api id',
        max_length=1000
    )
    title = models.CharField(
        'Titre du film',
        max_length=1000
    )
    img_path = models.CharField(
        'Chemin vers l\'image',
        max_length=1000
    )
    added_date = models.CharField(
        'Date d\'ajout',
        max_length=100
    )
    comment = models.CharField(
        'Commentaires',
        max_length=1000,
        blank=True
    )
    last_seen_episode = models.CharField(
        'Dernier episode vu',
        max_length=1000,
        blank=True
    )
    rate = models.DecimalField(
        'Note sur 10',
        max_digits=4,
        decimal_places=2
    )
    plan_to_watch = models.CharField(
        'WatchList',
        default=False
    )

    # Dependencies
    user = models.ForeignKey(
        RTUser,
        on_delete=models.CASCADE
    )
    type = models.ForeignKey(
        TvType,
        on_delete=models.CASCADE
    )
    gender = models.ForeignKey(
        TvGender,
        on_delete=models.CASCADE
    )

    # Methods
    def __str__(self):
        return self.title
