from django.db import models


# Tv
class TvGender(models.Model):
    tv_gender_id = models.BigIntegerField(
        'Type',
    )
    name = models.CharField(
        'Nom',
        max_length=500
    )

    def __str__(self):
        return self.name


class TvType(models.Model):
    name = models.CharField(
        'Nom',
        max_length=500
    )

    def __str__(self):
        return self.name


# Album
class AlbumGender(models.Model):
    album_gender_id = models.BigIntegerField(
        'Type',
    )
    name = models.CharField(
        'Nom',
        max_length=500
    )

    def __str__(self):
        return self.name
