from django.db import models

from apps.home.models.enumeration.models import AlbumGender
from apps.home.models.rt_user.models import RTUser


class Album(models.Model):
    # Fields
    album_id = models.CharField(
        'Api id',
        max_length=1000
    )
    title = models.CharField(
        'Titre de l\'album',
        max_length=1000
    )
    img_path = models.CharField(
        'Chemin vers l\'image',
        max_length=1000
    )
    added_date = models.CharField(
        'Date d\'ajout',
        max_length=100
    )
    comment = models.CharField(
        'Commentaires',
        max_length=1000,
        blank=True
    )
    rate = models.DecimalField(
        'Note sur 10',
        max_digits=4,
        decimal_places=2
    )
    plan_to_listen = models.CharField(
        'ListenList',
        default=False
    )

    # Preferred titles

    preferred_title_1 = models.CharField(
        'Top 1 titre',
        max_length=1000,
        blank=True
    )
    preferred_title_2 = models.CharField(
        'Top 2 titre',
        max_length=1000,
        blank=True
    )
    preferred_title_3 = models.CharField(
        'Top 3 titre',
        max_length=1000,
        blank=True
    )
    preferred_title_4 = models.CharField(
        'Top 4 titre',
        max_length=1000,
        blank=True
    )
    preferred_title_5 = models.CharField(
        'Top 5 titre',
        max_length=1000,
        blank=True
    )

    # Dependencies
    user = models.ForeignKey(
        RTUser,
        on_delete=models.CASCADE
    )
    gender = models.ForeignKey(
        AlbumGender,
        on_delete=models.CASCADE
    )

    # Methods
    def __str__(self):
        return self.title
